# NgChapterCoffee

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 18.0.1.

## Inicialización del proyecto

Para inicializar el proyecto, se debe ejecutar el comando `npm install` en la raíz del proyecto. Este comando instalará todas las dependencias necesarias para ejecutar el proyecto.

## Servidor de desarrollo

Para ejecutar el servidor de desarrollo, se debe ejecutar el comando `npm start`. Luego, se debe navegar a `http://localhost:4200/`. La aplicación se recargará automáticamente si se cambia alguno de los archivos fuente.

**Nota:** Este proyecto requiere de un servidor de backend para funcionar correctamente. Para más información, ver el archivo `README.md` en el proyecto de backend. Además de esto, requiere [Node.js](https://nodejs.org/en) v20+ y npm para funcionar.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.dev/tools/cli) page.
