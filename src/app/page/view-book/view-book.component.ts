import { Component } from '@angular/core';
import { BookService } from '../../core/services/book.service';
import { Book } from '../../core/interfaces/book';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CurrencyPipe } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@Component({
  selector: 'app-view-book',
  standalone: true,
  imports: [
    CurrencyPipe,
    MatButtonModule,
    MatIconModule,
    RouterLink,
    MatChipsModule,
    MatProgressSpinnerModule,
  ],
  templateUrl: './view-book.component.html',
  styleUrl: './view-book.component.scss',
})
export class ViewBookComponent {
  book!: Book;
  idRoute!: string;

  constructor(
    private _books: BookService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.idRoute = this.route.snapshot.params['id'];
    if (!this.idRoute) {
      this.toastr.warning('El libro que buscas no existe', '¡Ups!');
      this.router.navigate(['/']);
    }
    this._books.getBook(this.idRoute).subscribe({
      next: (book) => {
        this.book = book;
      },
      error: (error) => {
        this.toastr.error('Ha ocurrido un error', '¡Ups!');
        this.router.navigate(['/']);
      },
    });
  }

  addToCart(book: Book) {
    // TODO: Implement this method
    this.toastr.success('Agregado al carrito', '¡Listo!');
  }
}
