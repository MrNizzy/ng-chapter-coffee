import { Component } from '@angular/core';
import { LogoComponent } from '../../components/logo/logo.component';
import { BooksComponent } from '../../components/books/books.component';
import { ConfettiService } from '../../core/services/confetti.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [LogoComponent, BooksComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
})
export class HomeComponent {
  constructor(
    private _confetti: ConfettiService,
    private _snackbar: MatSnackBar
  ) {}

  celebrate(): void {
    this._confetti.celebrate();
    this.pizzaTime();
  }

  pizzaTime(): void {
    //random 1 o 2
    const random = Math.floor(Math.random() * 2) + 1;
    if (random === 1) {
      this._snackbar.open('¡Es hora de la pizza! 🍕', 'Cerrar', {
        duration: 3000,
      });
    }
    if (random === 2) {
      this._snackbar.open(
        '¡Es hora de un café y un buen libro! ☕📚',
        'Cerrar',
        {
          duration: 3000,
        }
      );
    }
  }
}
