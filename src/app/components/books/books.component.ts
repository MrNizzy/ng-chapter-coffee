import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import {
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
  MatFormFieldModule,
} from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { Book } from '../../core/interfaces/book';
import { Category } from '../../core/interfaces/category';
import { Author } from '../../core/interfaces/author';
import { BookService } from '../../core/services/book.service';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { CurrencyPipe, DatePipe } from '@angular/common';
import { CategoryService } from '../../core/services/category.service';
import { AuthorService } from '../../core/services/author.service';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { RouterLink } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {
  MatNativeDateModule,
  provideNativeDateAdapter,
} from '@angular/material/core';

@Component({
  selector: 'app-books',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    RouterLink,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    DatePipe,
    CurrencyPipe,
    MatTooltipModule,
  ],
  templateUrl: './books.component.html',
  styleUrl: './books.component.scss',
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        subscriptSizing: 'dynamic',
      },
    },
  ],
})
export class BooksComponent implements OnInit {
  categories: Category[] = [];
  authors: Author[] = [];
  books: Book[] = [];
  searchForm!: FormGroup;

  constructor(
    private _books: BookService,
    private fb: FormBuilder,
    private _categories: CategoryService,
    private _authors: AuthorService,
    private toastr: ToastrService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.searchForm = this.fb.group({
      title: [null],
      author: [null],
      category: [null],
      ascending: [true],
    });
    this.applyFilters();
    this.getCategories();
    this.getAuthors();
  }

  applyFilters(): void {
    const filters = {
      per_page: 10,
      author_id: this.searchForm.value.author,
      category_id: this.searchForm.value.category,
      title: this.searchForm.value.title,
      ascending: this.searchForm.value.ascending,
    };
    this._books.getBooks(filters).subscribe({
      next: (response) => {
        this.books = response.data;
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

  getCategories(): void {
    this._categories.getAllCategories().subscribe({
      next: (response) => {
        this.categories = response;
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

  getAuthors(): void {
    this._authors.getAllAuthors().subscribe({
      next: (response) => {
        this.authors = response;
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

  viewBook(id: number): void {
    console.log(id);
  }

  addToCart(id: number): void {
    // TODO: Implement cart service
    console.log(id);
    this.toastr.warning('Funcionalidad no implementada', 'Advertencia');
  }

  newBook(): void {
    const dialogRef = this.dialog.open(NewBookComponent, {
      width: '800px',
    });
    dialogRef.afterClosed().subscribe((result) => {
      this.applyFilters();
    });
  }
}

@Component({
  selector: 'app-new-book',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatCardModule,
    DatePipe,
    CurrencyPipe,
    MatTooltipModule,
    MatDialogModule,
    MatNativeDateModule,
    MatDatepickerModule,
  ],
  templateUrl: './new-book.component.html',
  styleUrl: './new-book.component.scss',
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        subscriptSizing: 'dynamic',
      },
    },
    provideNativeDateAdapter(),
  ],
})
export class NewBookComponent implements OnInit {
  categories: Category[] = [];
  authors: Author[] = [];
  bookForm!: FormGroup;

  constructor(
    private _books: BookService,
    private fb: FormBuilder,
    private _categories: CategoryService,
    private _authors: AuthorService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    //prettier-ignore
    this.bookForm = this.fb.group({
      title: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(254)]],
      description: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(511)]],
      price: [null, [Validators.required, Validators.min(0), Validators.max(9999999), Validators.pattern('^[0-9]+(\.[0-9]{1,2})?$')]],
      ids_authors: [null, [Validators.required]],
      ids_categories: [null, [Validators.required]],
      ISBN: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(13), Validators.pattern('^[0-9]{10,13}$')]],
      image_url: [null, [Validators.required, Validators.pattern('^(http|https)://.*$')]],
      status: [null, [Validators.required]],
      date_published: [null, [Validators.required]],
    });
    this.getCategories();
    this.getAuthors();
  }

  getCategories(): void {
    this._categories.getAllCategories().subscribe({
      next: (response) => {
        this.categories = response;
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

  getAuthors(): void {
    this._authors.getAllAuthors().subscribe({
      next: (response) => {
        this.authors = response;
      },
      error: (error) => {
        console.error(error);
      },
    });
  }

  saveBook(): void {
    console.log(this.bookForm.value);
    this._books.createBook(this.bookForm.value).subscribe({
      next: (response) => {
        console.log(response);
        this.toastr.success('Libro creado', 'Éxito');
      },
      error: (error) => {
        console.error(error);
        this.toastr.error('Error al crear libro', 'Error');
      },
    });
  }
}
