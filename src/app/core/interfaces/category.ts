import { Book } from './book';
import { Link, Pivot } from './pagination';

export interface Category {
  id: number;
  name: string;
  created_at: string;
  updated_at: string;
  books?: Book[] | null;
  pivot: Pivot;
}

export interface GetCategoriesResponse {
  current_page: number;
  data: Category[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  links: Link[];
  next_page_url: null;
  path: string;
  per_page: string;
  prev_page_url: null;
  to: number;
  total: number;
}
