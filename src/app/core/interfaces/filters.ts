export interface Filters {
  per_page?: number | null;
  category_id?: number | null;
  author_id?: number | null;
  title?: string | null;
  ascending?: boolean | null;
}
