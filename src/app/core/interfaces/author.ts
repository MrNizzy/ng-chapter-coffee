import { Link } from './pagination';

export interface Author {
  id: number;
  name: string;
  created_at: string;
  updated_at: string;
}

export interface GetAuthorsResponse {
  current_page: number;
  data: Author[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  links: Link[];
  next_page_url: null;
  path: string;
  per_page: string;
  prev_page_url: null;
  to: number;
  total: number;
}
