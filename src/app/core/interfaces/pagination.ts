export interface Pagination {}

export interface Pivot {
  id_book: number;
  id_category: number;
}

export interface Link {
  url: null | string;
  label: string;
  active: boolean;
}
