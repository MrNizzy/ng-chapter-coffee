import { Author } from './author';
import { Category } from './category';
import { Link } from './pagination';

export interface GetBooksResponse {
  current_page: number;
  data: Book[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  links: Link[];
  next_page_url: null;
  path: string;
  per_page: string;
  prev_page_url: null;
  to: number;
  total: number;
}

export interface Book {
  id: number;
  title: string;
  description: string;
  date_published: string;
  ISBN: string;
  price: number;
  image_url: string;
  status: string;
  created_at: string;
  updated_at: string;
  categories: Category[];
  authors: Author[];
}
