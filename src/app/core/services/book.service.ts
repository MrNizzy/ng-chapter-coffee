import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment.development';
import { Filters } from '../interfaces/filters';
import { Observable } from 'rxjs';
import { Book, GetBooksResponse } from '../interfaces/book';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  constructor(private http: HttpClient) {}

  getBooks(filters?: Filters): Observable<GetBooksResponse> {
    let url = `${environment.API_URL}books`;

    const queryParams = [];

    if (filters) {
      if (filters.per_page) {
        queryParams.push(`per_page=${filters.per_page}`);
      }
      if (filters.author_id) {
        queryParams.push(`author_id=${filters.author_id}`);
      }
      if (filters.category_id) {
        queryParams.push(`category_id=${filters.category_id}`);
      }
      if (filters.title) {
        queryParams.push(`title=${filters.title}`);
      }
      if (filters.ascending) {
        queryParams.push(`ascending=${filters.ascending}`);
      }
    }

    if (queryParams.length > 0) {
      url += `?${queryParams.join('&')}`;
    }

    return this.http.get<GetBooksResponse>(url);
  }

  getBook(id: number | string): Observable<Book> {
    return this.http.get<Book>(environment.API_URL + 'books/' + id);
  }

  createBook(data: FormData) {
    return this.http.post(environment.API_URL + 'books', data);
  }
}
