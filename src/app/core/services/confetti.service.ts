import { Injectable } from '@angular/core';
import confetti from 'canvas-confetti';

@Injectable({
  providedIn: 'root',
})
export class ConfettiService {
  constructor() {}

  celebrate() {
    const duration = 3000;

    // Lanzamiento de confeti rojo y azul desde el centro de la pantalla
    confetti({
      particleCount: 150,
      spread: 180,
      origin: { x: 0.5, y: 0.5 },
      colors: ['#bf0413', '#0477bf'],
      shapes: ['circle', 'square'],
    });

    setTimeout(() => confetti.reset(), duration);
  }
}
