import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment.development';
import { Observable } from 'rxjs';
import { Category } from '../interfaces/category';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private http: HttpClient) {}

  getAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(`${environment.API_URL}categories/all`);
  }

  getCategory(id: number) {
    return this.http.get(environment.API_URL + 'categories/' + id);
  }

  createCategory(data: FormData) {
    return this.http.post(environment.API_URL + 'categories', data);
  }
}
