import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment.development';
import { Observable } from 'rxjs';
import { Author } from '../interfaces/author';

@Injectable({
  providedIn: 'root',
})
export class AuthorService {
  constructor(private http: HttpClient) {}

  getAllAuthors(): Observable<Author[]> {
    return this.http.get<Author[]>(`${environment.API_URL}authors/all`);
  }

  getAuthor(id: number) {
    return this.http.get(environment.API_URL + 'authors/' + id);
  }

  createAuthor(data: FormData) {
    return this.http.post(environment.API_URL + 'authors', data);
  }
}
