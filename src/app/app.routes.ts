import { Routes } from '@angular/router';
import { HomeComponent } from './page/home/home.component';
import { ViewBookComponent } from './page/view-book/view-book.component';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    title: 'Chapter Coffee - Tienda de libros',
  },
  {
    path: 'book/:id',
    component: ViewBookComponent,
    title: 'Chapter Coffee - Detalle del libro',
  },
];
